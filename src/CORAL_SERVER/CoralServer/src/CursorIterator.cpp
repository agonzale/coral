// Include files
#include <iostream>
#include "CoralServerBase/InternalErrorException.h"
#include "CoralServerBase/../src/debug2936.h"
#include "RelationalAccess/ICursor.h"
#include "RelationalAccess/IQuery.h"

// Local include files
#include "CursorIterator.h"
#include "monitoring.h"

// Logger
//#define LOGGER_NAME "CoralServer::CursorIterator"
//#include "logger.h"

// Namespace
using namespace coral::CoralServer;

//-----------------------------------------------------------------------------

CursorIterator::CursorIterator( IQuery* query,
                                const std::shared_ptr<ISessionProxy> session )
  : m_pQuery( query )
  , m_cursor( query->execute() )
  , m_session( session )
{
}

//-----------------------------------------------------------------------------

CursorIterator::~CursorIterator()
{
  if ( m_pQuery )
  {
    m_cursor.close();
    delete m_pQuery;
    m_pQuery = 0;
  }
}

//-----------------------------------------------------------------------------

bool CursorIterator::nextRow()
{
  if ( debug2936 ) std::cout << "__Enter CursorIterator::nextRow" << std::endl; // debug CORALCOOL-2936
  try
  {
    SCOPED_TIMER( "ServerFacade::CursorIterator::nextRow" );
    if ( !m_pQuery ) return false;
    bool hasNext = m_cursor.next();
    if ( !hasNext )
    {
      m_cursor.close();
      delete m_pQuery;
      m_pQuery = 0;
    }
    if ( debug2936 ) std::cout << "__Exit CursorIterator::nextRow" << std::endl; // debug CORALCOOL-2936
    return hasNext;
  }
  catch( std::exception& e )
  {
    if ( debug2936 ) std::cout << "__Exception caught in CursorIterator::nextRow: " << e.what() << std::endl; // debug CORALCOOL-2936
    throw;
  }
}

//-----------------------------------------------------------------------------

bool CursorIterator::isLast() const
{
  throw InternalErrorException( "PANIC! CursorIterator::isLast() is not implemented and should never be called!",
                                "CursorIterator::isLast()",
                                "coral::CoralServer" );
}

//-----------------------------------------------------------------------------

const coral::AttributeList& CursorIterator::currentRow() const
{
  if ( !m_pQuery )
    throw Exception( "Iterator is already past the end",
                     "CursorIterator::currentRow",
                     "coral::CoralServer" );
  return m_cursor.currentRow();
}

//-----------------------------------------------------------------------------
