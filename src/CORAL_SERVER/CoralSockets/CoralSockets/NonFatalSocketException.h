#ifndef CORALSOCKETS_NONFATALSOCKETEXCEPTION_H
#define CORALSOCKETS_NONFATALSOCKETEXCEPTION_H 1

// Include files
#include "CoralServerBase/CoralServerBaseException.h"

namespace coral {

  namespace CoralSockets {

    class NonFatalSocketException : public CoralServerBaseException
    {

    public:

      /// Constructor
      NonFatalSocketException( const std::string& message,
                               const std::string& methodName = "" )
        : CoralServerBaseException( message, methodName, "coral::CoralSockets" ) {}

      /// Destructor
      virtual ~NonFatalSocketException() throw() {}

    };

  }

}
#endif
