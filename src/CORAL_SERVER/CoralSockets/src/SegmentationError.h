#ifndef CORALSOCKETS_SEGMENTATIONERROR_H
#define CORALSOCKETS_SEGMENTATIONERROR_H 1

// Include files
#include "CoralServerBase/CoralServerBaseException.h"

namespace coral {

  namespace CoralSockets {

    class SegmentationErrorException : public CoralServerBaseException
    {

    public:

      /// Constructor
      SegmentationErrorException( const std::string& what,
                                  const std::string& methodName = "" )
        : CoralServerBaseException( what, methodName, "coral::CoralSockets" ) {}

      /// Destructor
      virtual ~SegmentationErrorException() throw() {}

    };
  }
}
#endif // CORALSOCKETS_SEGMENTATIONERROR_H
