// Include files
#if 0
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/tcp.h> //  for TCP_NODELAY
#include <fcntl.h>
#include <netdb.h>
#endif
#include <cstring>
#include <iostream>
#include <vector>
#if 0
#include <sstream>
#endif
#include <fcntl.h>
#include <poll.h>
#include <unistd.h>
#include <sys/errno.h>
#include "CoralSockets/GenericSocketException.h"
#include "CoralSockets/NonFatalSocketException.h"
#include "Poll.h"

#define LOGGER_NAME "CoralSockets::Poll"
#include "logger.h"

// Enable verbose printouts on MacOSX if requested (debug CORALCOOL-1156)
#ifndef __APPLE__
#undef DEBUG
#define DEBUG( out )
#endif

// Namespace
using namespace coral::CoralSockets;

namespace 
{
  // open a pipe and put both ends in non-blocking mode
  int openPipe(int pipefd[2])
  {
    if (::pipe( pipefd ) == 0) {
      for (int i =0; i != 2; ++i) {
        int flags = ::fcntl(pipefd[i], F_GETFL, 0);
        if ( ::fcntl(pipefd[i], F_SETFL, flags | O_NONBLOCK) )
          ERROR( "Poll: openPipe() failed to set non-blocking mode" );
      }
      return 0;
    } else {
      pipefd[0] = -1;
      pipefd[1] = -1;
      return -1;
    }
  }
}

//---------------------------------------------------------------------------

namespace coral {

  namespace CoralSockets {

    class PollFDs {
    public:
      PollFDs( unsigned int maxFDs );
      ~PollFDs(){}

      void addSocket( int fd, const ISocketPtr& socket, Poll::PMode mode );

      void removeAt(unsigned pos);
      void removeSocket( const ISocketPtr& socket );

      void updateSocket( const ISocketPtr& socket, Poll::PMode mode );

      int size() const { return m_sockets.size(); }

      std::vector<struct pollfd>& getPollFDs()
      {
        return m_pollFD;
      }

      const std::vector<ISocketPtr>& getSockets() const
      {
        return m_sockets;
      }

    private:

      static int mode2events(Poll::PMode mode);

      std::vector<struct pollfd> m_pollFD;

      std::vector<ISocketPtr> m_sockets;
    };
  }
}

//---------------------------------------------------------------------------

PollFDs::PollFDs(unsigned int maxFDs)
{
  m_pollFD.reserve(maxFDs);
  m_sockets.reserve(maxFDs);
}

//---------------------------------------------------------------------------

void PollFDs::addSocket(int fd, const ISocketPtr& socket, Poll::PMode mode)
{
  // check that socket is not already added
  if (std::find(m_sockets.begin(), m_sockets.end(), socket) != m_sockets.end()) {
    throw GenericSocketException("socket allready in PollFDs", "addSocket");
  }

  pollfd pfd;
  pfd.fd = fd;
  pfd.events = mode2events(mode);
  DEBUG("socket " << pfd.fd << " pmode " << mode << " events " << pfd.events );

  m_pollFD.push_back(pfd);
  m_sockets.push_back(socket);
}

//---------------------------------------------------------------------------

void PollFDs::removeAt(unsigned pos)
{
  if (pos != m_sockets.size()-1) {
    // copy last item to the position of the removed item
    *(m_sockets.begin() + pos) = *(m_sockets.end() - 1);
    *(m_pollFD.begin() + pos) = *(m_pollFD.end() - 1);
  }
  // remove last item
  m_sockets.resize(m_sockets.size() - 1);
  m_pollFD.resize(m_pollFD.size() - 1);
}

//---------------------------------------------------------------------------
void PollFDs::removeSocket(const ISocketPtr& socket )
{
  std::vector<ISocketPtr>::iterator itr =
    std::find(m_sockets.begin(), m_sockets.end(), socket);
  if (itr == m_sockets.end()) {
    throw GenericSocketException("socket not found in PollFDs","removeSocket");
  }

  removeAt(itr - m_sockets.begin());
}

//---------------------------------------------------------------------------

void PollFDs::updateSocket(const ISocketPtr& socket, Poll::PMode mode)
{
  std::vector<ISocketPtr>::iterator itr =
    std::find(m_sockets.begin(), m_sockets.end(), socket);
  if (itr == m_sockets.end()) {
    throw GenericSocketException("socket not found in PollFDs", "updateSocket");
  }

  int pos = itr - m_sockets.begin();
  m_pollFD[pos].events = mode2events(mode);
}

//---------------------------------------------------------------------------
int PollFDs::mode2events(Poll::PMode mode)
{
  int events = 0;
  switch (mode) {
  case Poll::P_READ:
    events = POLLIN;
    break;
  case Poll::P_WRITE:
    events = POLLOUT;
    break;
  case Poll::P_READ_WRITE:
    events = POLLOUT | POLLIN;
    break;
  case Poll::P_NONE:
    events = 0;
    break;
  }
  return events;
}

//---------------------------------------------------------------------------

Poll::Poll( unsigned int maxFDs )
  : m_pollFD( new PollFDs( maxFDs ) )
{
  INFO("Poll constructor ");
  m_currentSocket = -1;
  m_maxReadySockets = 0;

  // open a pipe for signaling
  if (::openPipe( m_pipefd ) != 0) {
    INFO("Poll: pipe() has failed, polling preemptive stopping will not work");
  } else {
    // add it as the first item in the polling list
    m_pollFD->addSocket( m_pipefd[0], ISocketPtr(), P_READ );
  }
}

//---------------------------------------------------------------------------

Poll::~Poll()
{
  // close the pipe
  if (m_pipefd[0] >= 0) {
    close(m_pipefd[0]);
    close(m_pipefd[1]);
  }
}

//---------------------------------------------------------------------------

void Poll::addSocket( const ISocketPtr& socket, Poll::PMode mode )
{
  m_pollFD->addSocket( socket->getFd(), socket, mode );
}

//---------------------------------------------------------------------------

void Poll::removeSocket( const ISocketPtr& socket )
{
  // reset any open iterators
  m_currentSocket = -1;
  m_maxReadySockets = 0;

  m_pollFD->removeSocket( socket );
}

//---------------------------------------------------------------------------

void Poll::updateSocket( const ISocketPtr& socket, PMode mode )
{
  m_pollFD->updateSocket( socket, mode );
}

//---------------------------------------------------------------------------

int Poll::poll( int timeout )
{
  // reset iterator
  m_currentSocket=-1;
  m_maxReadySockets = 0;

  std::vector<struct pollfd>& pfd = m_pollFD->getPollFDs();
  int count = pfd.size();

  DEBUG("calling poll with " << count << " sockets ");
  //std::cout << "poll fd " << m_pollFD->getPollFDs()[0].fd << " status " << m_pollFD->getPollFDs()[0].events << std::endl;
  int ret = ::poll( &pfd[0], count, timeout );
  DEBUG("returned from poll ret value " << ret );
  //std::cout << "poll fd " << m_pollFD->getPollFDs()[0].fd << " rstatus " << m_pollFD->getPollFDs()[0].revents << std::endl;

  if (ret < 0 ) {
    if (errno == EINTR)
      // not fatal error
      // and there is no really correct way to handle interrupts
      // we just return 0 and hope to get called again
      return 0;

    std::stringstream errStr;
    errStr << "Error in Poll::poll() :'" <<strerror(errno) << "'";
    throw GenericSocketException( errStr.str() );
  }

  if (m_pipefd[0] >= 0) {

    // do not count signaling fd in returned signals
    if (pfd[0].revents) {
      -- ret;
    }

    // if there is any error condition in signaling fd then try to re-open the pipe
    // or remove it from polling
    if (pfd[0].revents & ~POLLIN) {
      INFO("Poll: signaling pipe has exceptional condition, will try to reopen: revents = " << pfd[0].revents);
      close(m_pipefd[0]);
      close(m_pipefd[1]);
      if (::openPipe( m_pipefd ) != 0) {
        INFO("Poll: pipe() has failed, polling preemptive stopping will not work");
        m_pollFD->removeAt(0);
      } else {
        pfd[0].fd = m_pipefd[0];
        pfd[0].revents = 0;
      }
    }
  }

  if (m_pipefd[0] >= 0) {
    // ignore signaling socket
    ++ m_currentSocket;

    // try to read several bytes from termination pipe (there could be several stop requests)
    if (pfd[0].revents & POLLIN) {
      char buf[32];
      // this will read up to 32 bytes, and we do not care if it fails
      ::read(m_pipefd[0], buf, sizeof buf); // Coverity CHECKED_RETURN (ignore!)
    }

  }

  m_maxReadySockets = ret;
  return ret;
}

//---------------------------------------------------------------------------
void Poll::stopPoll()
{
  // write single byte into pipe, this will trigger poll event
  if (m_pipefd[1] >= 0) {
    // do not actually care if this fails, will rely on timeouts
    ::write(m_pipefd[1], "X", 1);
  }
}

//---------------------------------------------------------------------------

const ISocketPtr& Poll::getNextReadySocket()
{
  if ( m_maxReadySockets <= 0 )
    throw GenericSocketException("no more sockets are ready",
                                 "Poll::getNextReadySocket");

  std::vector<struct pollfd>& fds = m_pollFD->getPollFDs();

  // advance at least one socket
  m_currentSocket++;
  while ( m_currentSocket < m_pollFD->size() &&
          fds[m_currentSocket].revents == 0 )
    m_currentSocket++;

  if ( m_currentSocket >= m_pollFD->size() )
    throw GenericSocketException("no more sockets in array");

  m_maxReadySockets--;
  return m_pollFD->getSockets()[ m_currentSocket ];
}

//-------------------------------------------------------------------------

bool Poll::currSocketClosed()
{
  std::vector<struct pollfd>& fds = m_pollFD->getPollFDs();
  return ( fds[ m_currentSocket ].revents & ( POLLHUP | POLLNVAL ) ) != 0;
}
