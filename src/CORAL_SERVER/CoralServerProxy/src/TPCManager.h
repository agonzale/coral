#ifndef CORALSERVERPROXY_TPCMANAGER_H
#define CORALSERVERPROXY_TPCMANAGER_H

// --------------------------------------------------------------------------
// File and Version Information:

// Description:
	// Class TPCManager.

// ------------------------------------------------------------------------

// -----------------
// C/C++ Headers --
// -----------------
#include <string>
#include <vector>

// ----------------------
// Base Class Headers --
// ----------------------

// -------------------------------
// Collaborating Class Headers --
// -------------------------------
#include "NetEndpointAddress.h"
#include "NetSocket.h"

// ------------------------------------
// Collaborating Class Declarations --
// ------------------------------------

		// ---------------------
		// -- Class Interface --
		// ---------------------

/**
 *  "Runnable" class (in terms of Boost.thread) which handles the
 *  connections from clients and instantiates all the threads.
 *  This implements thread-per-connection (TPC) model, instantiating
 *  new SingleClientReader thread for every new incoming connection.
 *  It also creates all communication/sinchronization queues and
 *  reader/writer/dispatcher which communicate through these queues.
 *
 *  @version $Id: TPCManager.h,v 1.1.2.1 2010-05-26 08:12:41 avalassi Exp $
 *
 *  @author Andy Salnikov
 */

namespace coral {
namespace CoralServerProxy {

class IPacketCache ;
class StatCollector ;

class TPCManager  {
public:

  // Default constructor
  TPCManager ( const NetEndpointAddress& listenAddress,
               bool overridePortmap,
               const std::vector<NetEndpointAddress>& serverAddress,
               size_t maxQueueSize,
               unsigned timeout,
               IPacketCache& cache,
               StatCollector* statCollector,
               bool keepUpstreamOpen,
               unsigned maxAccepErrors,
               bool unregister_portmap=true,
               const std::string& pmap_lock_dir=std::string()) ;

  // Destructor
  ~TPCManager () ;

  // run method runs until signal is received.
  // returns 0 on success (when stopped by signal) or
  // negative number in case of errors.
  int run() ;

protected:

  // start listening
  NetSocket setupListener() ;

  // set common socket options
  int setSocketOptions( NetSocket& sock ) ;

private:

  // Data members
  NetEndpointAddress m_listenAddress ;   // listenning iface address
  bool m_overridePortmap;
  std::vector<NetEndpointAddress> m_serverAddress ;  // upstream server address
  size_t m_maxQueueSize ;
  unsigned m_timeout ;
  IPacketCache& m_cache;
  StatCollector* m_statCollector ;
  bool m_keepUpstreamOpen;
  unsigned m_maxAccepErrors;
  bool m_unregisterPortmap;
  std::string m_pmapLockDir;
};

} // namespace CoralServerProxy
} // namespace coral

#endif // CORALSERVERPROXY_TPCMANAGER_H
