#ifndef CORALSERVERBASE_CORALSERVERPROXYEXCEPTION_H
#define CORALSERVERBASE_CORALSERVERPROXYEXCEPTION_H 1

// Include files
#include "CoralServerBase/CoralServerBaseException.h"

namespace coral
{

  /** @class CoralServerProxyException
   *
   *  Exception thrown by the CoralServerProxy package.
   *
   *  @author Andrea Valassi, Alexander Kalkhof and Martin Wache
   *  @date   2009-02-11
   */

  class CoralServerProxyException : public CoralServerBaseException
  {

  public:

    /// Constructor
    CoralServerProxyException( const std::string& message,
                               const std::string& method )
      : CoralServerBaseException( message, method, "coral::CoralServerProxy" ){}

    /// Destructor
    virtual ~CoralServerProxyException() throw() {}

    /// Encode the exception as a CAL packet payload
    const std::string asCALPayload() const;

  };

}
#endif // CORALSERVERBASE_CORALSERVERPROXYEXCEPTION_H
