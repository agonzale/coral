#ifndef CORALSERVERBASE_NOTIMPLEMENTED_H
#define CORALSERVERBASE_NOTIMPLEMENTED_H 1

// Include files
#include "CoralServerBase/CoralServerBaseException.h"

namespace coral
{

  /** @class NotImplemented
   *
   *  @author Andrea Valassi
   *  @date   2007-12-04
   *///

  class NotImplemented : public CoralServerBaseException
  {

  public:

    /// Constructor
    NotImplemented( const std::string& methodName,
                    const std::string& moduleName = "coral::CoralAccess" )
      : CoralServerBaseException( "This method is not yet implemented",
                                  methodName,
                                  moduleName ) {}

    /// Destructor
    virtual ~NotImplemented() throw() {}

  };

}
#endif // CORALSERVERBASE_NOTIMPLEMENTED_H
