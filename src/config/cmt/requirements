package CORAL

#============================================================================
# Project components
#============================================================================

# Define CMTINSTALLAREA correctly
use LCG_Policy v*

#----------------------------------------------------------------------------
# The packages providing public API
#----------------------------------------------------------------------------

use CoralBase *
use RelationalAccess *

#----------------------------------------------------------------------------
# The packages providing the developer-level interfaces
#----------------------------------------------------------------------------

use CoralKernel *
use CoralCommon *
use PyCoral     *

#----------------------------------------------------------------------------
# The packages providing the plugin libraries
#----------------------------------------------------------------------------

use ConnectionService *
use RelationalService *
use MonitoringService *
use XMLAuthenticationService *
use EnvironmentAuthenticationService *
use XMLLookupService *

# LFCReplicaSvc only on Linux (except SLC6, see bug #86877)
# [This has been dropped as of LCCMT_63]
###macro select_lfcreplicaservice  "LFCReplicaService *" \
###      target-slc6 "" \
###      target-darwin "" \
###      target-winxp ""
###use $(select_lfcreplicaservice)

use OracleAccess *
use MySQLAccess *
use SQLiteAccess *

# Frontier on all platforms but windows
macro select_frontieraccess "FrontierAccess *" \
    target-winxp ""
use $(select_frontieraccess) 

#----------------------------------------------------------------------------
# The CORAL_SERVER components (only on Linux SLC5/SLC6 and OSX)
#----------------------------------------------------------------------------
# [NB Could also use CORAL_SERVER directly: it no longer sets CORAL_AUTH_PATH]

macro select_coralmonitor \
  "CoralMonitor * CORAL_SERVER" \
  target-slc4 "" \
  target-winxp ""
macro select_coralserverbase \
  "CoralServerBase * CORAL_SERVER" \
  target-slc4 "" \
  target-winxp ""
macro select_coralaccess \
  "CoralAccess * CORAL_SERVER" \
  target-slc4 "" \
  target-winxp ""
macro select_coralserver \
  "CoralServer * CORAL_SERVER" \
  target-slc4 "" \
  target-winxp ""
macro select_coralsockets \
  "CoralSockets * CORAL_SERVER" \
  target-slc4 "" \
  target-winxp ""
macro select_coralstubs \
  "CoralStubs * CORAL_SERVER" \
  target-slc4 "" \
  target-winxp ""
macro select_coralserverproxy \
  "CoralServerProxy * CORAL_SERVER" \
  target-slc4 "" \
  target-winxp ""
macro select_coralauthenticationservice \
  "CoralAuthenticationService * CORAL_SERVER" \
  target-slc4 "" \
  target-winxp ""

use $(select_coralmonitor)
use $(select_coralserverbase)
use $(select_coralaccess)
use $(select_coralserver)
use $(select_coralsockets)
use $(select_coralstubs)
use $(select_coralserverproxy)
use $(select_coralauthenticationservice)

#----------------------------------------------------------------------------
# The integration tests
#----------------------------------------------------------------------------

use Tests *

#============================================================================
# Private dependencies, build rules and runtime
#============================================================================

private

#----------------------------------------------------------------------------
# CORAL test runtime
#----------------------------------------------------------------------------

# NB Use this last as this also contains workarounds overriding LCGCMT
use CoralTest v*

#----------------------------------------------------------------------------
# Actions and build rules - install setup
#----------------------------------------------------------------------------

macro ProjectTest CoralTest
macro ProjectTest_cmtpath $(CoralTest_cmtpath)

macro cpdproption "-dpr" host-darwin "-pPR"
action install_cmt "if [ ! -d $(CMTINSTALLAREA)/$(tag)/cmt ]; then mkdir -p $(CMTINSTALLAREA)/$(tag)/cmt; cat $(config_cmtpath)/cmt/project.cmt | sed 's/use LCGCMT .*/use LCGCMT $(LCGCMT_config_version)/' > $(CMTINSTALLAREA)/$(tag)/cmt/project.cmt; fi"
action install_ProjectTest "if [ ! -d $(CMTINSTALLAREA)/$(tag)/$(ProjectTest) ]; then mkdir -p $(CMTINSTALLAREA)/$(tag); \cp $(cpdproption) $(ProjectTest_cmtpath)/$(ProjectTest) $(CMTINSTALLAREA)/$(tag)/$(ProjectTest); fi"
macro setupdir $(CMTINSTALLAREA)/$(tag)/bin
macro setupcsh $(setupdir)/setup$(config_project).csh
macro setupsh $(setupdir)/setup$(config_project).sh
action install_setup_scripts "mkdir -p $(setupdir); \rm -f $(setupcsh) $(setupsh); echo setenv CMTCONFIG $(tag) > $(setupcsh); echo export CMTCONFIG=$(tag) > $(setupsh); echo set CMTsrcdir=`basename $(config_cmtpath)` >> $(setupcsh); echo CMTsrcdir=`basename $(config_cmtpath)` >> $(setupsh); cat _setup.csh >> $(setupcsh); cat _setup.sh >> $(setupsh)"

# Append to 'constituents' to execute an action in 'cmt make'
# (append to 'all_constituents' to execute it only in 'cmt make all').
# Remove the action from cmt_actions_constituents so that the action 
# is not executed twice in 'cmt make all_groups' (it executes all actions).
macro_append constituents "install_setup_scripts install_cmt install_ProjectTest"
macro_remove cmt_actions_constituents "install_setup_scripts install_cmt install_ProjectTest"

#----------------------------------------------------------------------------
# Actions and build rules - tests, utilities and examples
#----------------------------------------------------------------------------

# Fake target for tests
action tests "echo No tests in this package"
macro_remove cmt_actions_constituents "tests"

# Fake target for utilities
action utilities "echo No utilities in this package"
macro_remove cmt_actions_constituents "utilities"

# Fake target for examples
action examples "echo No examples in this package"
macro_remove cmt_actions_constituents "examples"
 
#============================================================================
