#ifndef CORALBASE_CORALBINDHEADERS_H
#define CORALBASE_CORALBINDHEADERS_H 1

// NB: coral_bind_headers should be _completely_ removed in all branches!

// FIXME! Should use c++11 instead of boost in the internal implementation!
// NB: boost_bind_headers should be _completely_ removed in CORAL3 branch!
#include "CoralBase/../src/boost_bind_headers.h" // SHOULD BE REMOVED!

#endif // CORALBASE_CORALBINDHEADERS_H
