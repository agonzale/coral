#ifndef CORAL_CORALBASE_DATE_H
#define CORAL_CORALBASE_DATE_H 1

// Include files
#include <chrono> // replace Boost by c++11 (task #49014 and task #50598)

namespace coral
{
  /**
   * @class Date Date.h CoralBase/Date.h
   *
   * A class defining a DATE type.
   *
   *///
  class Date
  {
  public:

    /// Default constructor - Returns the current UTC date
    Date();

    /// Constructor
    Date( int year, int month, int day );

    /// Destructor
    ~Date();

    /// Constructor from a time_point
    explicit Date( const std::chrono::system_clock::time_point& );

    /// Copy constructor
    Date( const Date& rhs );

    /// Assignment operator
    Date& operator=( const Date& rhs );

    /// Equal operator
    bool operator==( const Date& rhs ) const;

    /// Comparison operator
    bool operator!=( const Date& rhs ) const;

    /// Returns the year
    int year() const;

    /// Returns the month [1-12]
    int month() const;

    /// Returns the day [1-31]
    int day() const;

    /// Returns a time_point (by value)
    std::chrono::system_clock::time_point time() const;

  private:

    /// The actual year, month [1-12] and day [1-31]
    int m_year, m_mon, m_day;

  };

}

/// Inline methods
inline int
coral::Date::year() const
{
  return m_year;
}


inline int
coral::Date::month() const
{
  return m_mon;
}


inline int
coral::Date::day() const
{
  return m_day;
}


inline bool
coral::Date::operator==( const coral::Date& rhs ) const
{
  return ( this->year() == rhs.year() &&
           this->month() == rhs.month() &&
           this->day() == rhs.day() );
}


inline bool
coral::Date::operator!=( const coral::Date& rhs ) const
{
  return ( this->year() != rhs.year() ||
           this->month() != rhs.month() ||
           this->day() != rhs.day() );
}
#endif
