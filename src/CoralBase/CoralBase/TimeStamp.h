#ifndef CORAL_CORALBASE_TIMESTAMP_H
#define CORAL_CORALBASE_TIMESTAMP_H 1

// Include files
#include <chrono> // replace Boost by c++11 (task #49014 and task #50598)
#include <ostream>
#include <string>

namespace coral
{

  /**
     @class TimeStamp TimeStamp.h CoralBase/TimeStamp.h

     A class defining the TIMESTAMP type.

     In CORAL 2.x both UTC and local timestamps are supported.
     In CORAL 3.x only UTC timestamps are supported.

  *///
  class TimeStamp
  {
  public:

    /// The int64 type for storing time as nanoseconds since the epoch time UTC
    typedef signed long long int ValueType;

  public:

    /// Default constructor, returns the current time UTC
    TimeStamp();

    /// Constructor from YYYY-MM-DD:hh:mm:ss.nnnnnnnnn UTC.
    /// Range: 1677-09-21_00:12:43.145224192 to 2262-04-11_23:47:16.854775807.
    /// Throws an Exception if input parameters are outside their ranges
    /// or if the day of that month does not exist during that year.
    /// [This is guaranteed to have no loss of precision on CORAL3.]
    /// [TimeStamp is guaranteed to have nanosecond resolution in CORAL3!]
    TimeStamp( int year, // [1677-2262] int64 range around 1970...
               int month, // [1-12]
               int day, // [1-31]
               int hour, // [0-23]
               int minute, // [0-59]
               int second, // [0-59]
               long nanosecond ); // [0-999999999]

    /// Constructor from a c++11 time_point UTC.
    /// [WARNING! This may lead to loss of precision on CORAL3 on icc13.]
    /// [Check resolution via typeid(system_clock::time_point::period).]
    /// [On icc13, c++11 time_point has microsecond resolution (bug #104622)!]
    explicit TimeStamp( const std::chrono::system_clock::time_point& );

    /// Constructor from number of nanoseconds since epoch time UTC.
    /// [This is guaranteed to have no loss of precision on CORAL3.]
    /// [TimeStamp is guaranteed to have nanosecond resolution in CORAL3!]
    explicit TimeStamp( ValueType nsecs );

    /// Destructor
    ~TimeStamp();

    /// Copy constructor
    TimeStamp( const TimeStamp& rhs );

    /// Assignment operator
    TimeStamp& operator=( const TimeStamp& rhs );

    /// Returns the year
    int year() const;

    /// Returns the month [1-12]
    int month() const;

    /// Returns the day [1-31]
    int day() const;

    /// Returns the hour [0-23]
    int hour() const;

    /// Returns the minute [0-59]
    int minute() const;

    /// Returns the second [0-59]
    int second() const;

    /// Returns the nanoseconds [0-999999999].
    /// [This is guaranteed to have no loss of precision on CORAL3.]
    /// [TimeStamp is guaranteed to have nanosecond resolution in CORAL3!]
    long nanosecond() const;

    /// The number of nanoseconds from epoch 01/01/1970 UTC (fits in int64)
    /// [This is guaranteed to have no loss of precision on CORAL3.]
    /// [TimeStamp is guaranteed to have nanosecond resolution in CORAL3!]
    ValueType total_nanoseconds() const;

    /// Returns a c++11 time_point UTC (by value).
    /// [WARNING! This may lead to loss of precision on CORAL3 on icc13.]
    /// [Check resolution via typeid(system_clock::time_point::period).]
    /// [On icc13, c++11 time_point has microsecond resolution (bug #104622)!]
    std::chrono::system_clock::time_point time() const;

    /// Equal operator
    bool operator==( const TimeStamp& rhs ) const;

    /// Comparison operator
    bool operator!=( const TimeStamp& rhs ) const;

    /// Comparison operator
    bool operator>( const TimeStamp& rhs ) const;

    /// Comparison operator
    bool operator>=( const TimeStamp& rhs ) const;

    /// Comparison operator
    bool operator<( const TimeStamp& rhs ) const;

    /// Comparison operator
    bool operator<=( const TimeStamp& rhs ) const;

    /// Return the current time UTC
    static TimeStamp now();

    /// Return the string representation of the time stamp
    std::string toString() const;

    /// Print to an output stream.
    std::ostream& print( std::ostream& os ) const;

  private:

    /// The actual year, month [1-12] and day [1-31]
    int m_year, m_mon, m_day;

    /// The actual hour, minute, second and nanosecond
    int m_hour, m_min, m_sec, m_nsec;

  };

  /// Print to an output stream.
  inline std::ostream& operator<<( std::ostream& s, const TimeStamp& time )
  {
    return time.print( s );
  }

}

#endif
