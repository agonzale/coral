#ifndef CORALKERNEL_PROPERTY_H
#define CORALKERNEL_PROPERTY_H 1

// Include files
#include <functional> // task #30840
#include <mutex> // task #49014
#include <map>
#include "CoralKernel/IProperty.h"

namespace coral
{
  /**
   * Class Property
   * Implementation of the IProperty interface.
   * Property objects own a mutex to provide thread safety.
   * The user of the property may register callbacks that are
   * executed whenever the set method is called.
   * The execution order of the callbacks are undefined.
   * The callbacks may be unregistered as well, and the corresponding
   * callbacks must be unregistered when the receiving object is deleted.
   *
   * The callbacks are identified by unique callback ID's.
   * The registering object must remember the callback ID
   * as that ID is used for unregistering the callback.
   *
   * The callbacks must be boost functors with a string input parameter that
   * will hold the new value of the property. There is no check for functor
   * equality, if a callback object is registered twice, it will be called
   * twice, there will be two corresponding callback ID's, and both ID's
   * must be unregistered.
   *///
  class Property
    : public IProperty
  {
  public:

    Property();
    virtual ~Property();
    virtual bool set(const std::string&);
    virtual const std::string& get() const;

    /**
     * Callback functor type.
     *///
    typedef std::function<void (const std::string&)> Callback;

    /**
     * Unique ID of the callback.
     *///
    typedef long int CallbackID;

    /**
     * Register a callback, and return the corresponding callback ID.
     *///
    CallbackID registerCallback(Callback&);

    /**
     * Unregister the callback corresponding to the callback ID. Return true
     * if the callback has been found and unregistered successfully else false.
     *///
    bool unregisterCallback(CallbackID&);

  private:

    std::string m_value;
    std::map<CallbackID, Callback> m_callbacks;
    CallbackID m_lastID;
    mutable std::mutex m_mutex;

  };

}

#endif // CORALKERNEL_PROPERTY_H
