#include <iostream>
#include "CoralKernel/Context.h"
#include "CoralKernel/ILoadableComponent.h"
#include "PluginManager.h"
#include "PropertyManager.h"


coral::Context&
coral::Context::instance()
{
  static coral::Context theInstance;
  return theInstance;
}


bool
coral::Context::existsComponent( const std::string& componentName )
{
  std::lock_guard<std::mutex> lock( m_mutex );
  if ( m_components.find( componentName ) != m_components.end() )
    return true;
  else
    return false;
}


void
coral::Context::loadComponent( const std::string& componentName,
                               coral::IPluginManager* pluginManager )
{
  /// Check if the component has already been loaded.
  if ( this->existsComponent( componentName ) ) return;

  /// Use the provided plugin manager or the default implementation
  coral::IPluginManager* thePluginManager = pluginManager;
  if ( thePluginManager == 0 )
  {
    std::lock_guard<std::mutex> lock( m_mutex );
    thePluginManager = m_pluginManager.get();
  }

  // Load the component
  ILoadableComponent* comp = thePluginManager->newComponent( componentName );
  if ( comp )
  {
    std::lock_guard<std::mutex> lock( m_mutex );
    m_components.insert( std::make_pair( componentName, comp ) );
  }
}


coral::IPropertyManager& coral::Context::propertyManager()
{
  return *m_propertyManager;
}


std::set<std::string>
coral::Context::loadedComponents() const
{
  std::set<std::string> componentSet;
  for ( std::map<std::string, ILoadableComponent*>::const_iterator
          iCom = m_components.begin(); iCom != m_components.end(); iCom++ )
    componentSet.insert( iCom->first );
  return componentSet;
}


std::set<std::string>
coral::Context::knownComponents() const
{
  std::set<std::string> componentSet = m_pluginManager->knownPlugins();
  return componentSet;
}


coral::Context::Context()
  : m_mutex()
  , m_components()
  , m_pluginManager( new coral::PluginManager )
  , m_propertyManager( new coral::PropertyManager )
{
  //std::cout << "Create Context " << this << std::endl;
}


coral::Context::~Context()
{
  //std::cout << "Delete Context " << this << "..." << std::endl;
  ILoadableComponent* pMonSvc = 0;
  // Fix destruction order of loaded components (bug #63042 and bug #73529).
  // Destruction order is normally the inverse of construction order,
  // but the CoralMonitoringService should always be deleted last.
  for ( std::map<std::string, ILoadableComponent*>::const_iterator
          iCom = m_components.begin(); iCom != m_components.end(); iCom++ )
  {
    std::string name = iCom->second->name();
    if( name == "CORAL/Services/CoralMonitoringService" )
    {
      pMonSvc = iCom->second; // todo: check pMonSvc==0? or names are unique?
    }
    else
    {
      // Delete all components except the monitoring service
      iCom->second->removeReference();
      //int count = iCom->second->removeReference();
      //std::cout << "RefCount-- " << iCom->second << " " << name
      //          << ": " << count << std::endl;
    }
  }
  // Delete the monitoring service last
  if( pMonSvc )
  {
    pMonSvc->removeReference();
    //int count = pMonSvc->removeReference();
    //std::string name = pMonSvc->name();
    //std::cout << "RefCount-- " << pMonSvc << " " << name
    //          << ": " << count << std::endl;
  }

  // Clear all components from the map
  m_components.clear();
  //std::cout << "Delete Context " << this << "... done" << std::endl;
}
