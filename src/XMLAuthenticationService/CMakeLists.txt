# Required external packages
find_package(XercesC REQUIRED)
include_directories(${XERCESC_INCLUDE_DIRS})

include(CORALModule)
CORALModule(LIBS lcg_CoralCommon ${XERCESC_LIBRARIES}
            TESTS LoadService)

# Install the authentication.xml for tests (CORALCOOL-896)
include(CORALConfigScripts)
copy_and_install_program(authentication.xml tests/LoadService tests/bin/XMLAuthenticationService/LoadService tests/bin/XMLAuthenticationService/LoadService)
copy_and_install_program(authentication1.xml tests/LoadService tests/bin/XMLAuthenticationService/LoadService tests/bin/XMLAuthenticationService/LoadService)
copy_and_install_program(authentication2.xml tests/LoadService tests/bin/XMLAuthenticationService/LoadService tests/bin/XMLAuthenticationService/LoadService)
