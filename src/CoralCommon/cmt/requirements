package CoralCommon

#============================================================================
# Public dependencies and build rules
#============================================================================

use CoralKernel v*
use RelationalAccess v*

apply_pattern lcg_shared_library

# Create a symlink .dylib -> .so on mac (sr #141482 - also see bug #37371)
apply_pattern lcg_dylib_symlink

#============================================================================
# Private dependencies and build rules
#============================================================================

private

use CppUnit v* LCG_Interfaces -no_auto_imports

macro_prepend CppUnit_linkopts " -Wl,--no-as-needed " target-mac ""
macro_append CppUnit_linkopts " -Wl,--as-needed " target-mac ""

# Do not install the header files (task #15556)
macro_remove constituents 'install_includes'

# The unit tests
apply_pattern coral_unit_test tname=MonitoringEvent
apply_pattern coral_unit_test tname=SimpleExpressionParser
apply_pattern coral_unit_test tname=Timer
apply_pattern coral_unit_test tname=URIParser

# Fake target for utilities
action utilities "echo No utilities in this package"
macro_remove cmt_actions_constituents "utilities"

# Fake target for examples
action examples "echo No examples in this package"
macro_remove cmt_actions_constituents "examples"
