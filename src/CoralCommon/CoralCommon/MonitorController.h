#ifndef CORALCOMMON_MONITORCONTROLLER_H
#define CORALCOMMON_MONITORCONTROLLER_H 1

#include <memory>
#include "CoralCommon/IDevSessionProperties.h"
#include "CoralCommon/IMonitoringController.h"

namespace coral
{

  namespace CoralCommon
  {

    /**
     * Class MonitorController
     *
     * Implementation of the IMonitoringController interface for SQLiteAccess.
     *
     *///
    class MonitorController : virtual public coral::IMonitoringController
    {

    public:

      /// Constructor
      explicit MonitorController( std::shared_ptr<const IDevSessionProperties> properties );

      /// Destructor
      virtual ~MonitorController();

      /**
       * Starts the client-side monitoring for the current session.
       * Throws a MonitoringServiceNotFoundException if there is no monitoring service available.
       *///
      void start( coral::monitor::Level level = coral::monitor::Default );

      /**
       * Stops the client side monitoring.
       * Throws a monitoring exception if something went wrong.
       *///
      void stop();

      /**
       * Reports whatever has been gather by the monitoring service to an std::ostream.
       * Throws a MonitoringServiceNotFoundException if there is no monitoring service available.
       *///
      void reportToOutputStream( std::ostream& os ) const;

      /**
       * Triggers the reporting of the underlying monitoring service.
       * Throws a MonitoringServiceNotFoundException if there is no monitoring service available.
       *///
      void report() const;

      /**
       * Returns the status of the monitoring.
       *///
      bool isActive() const;

    private:

      /// Pointer to the session properties
      std::shared_ptr<const IDevSessionProperties> m_properties;

      /// Domain service name (could also be extracted from session properties)
      /// Allows the same MonitorController for all plugins (CORALCOOL-2941)
      const std::string m_domainServiceName;

      /// Connection string (could also be extracted from session properties)
      /// Allows the same MonitorController for all plugins (CORALCOOL-2941)
      const std::string m_connectionString;
      
    };

  }

}
#endif // SQLITEACCESS_MONITORCONTROLLER_H
