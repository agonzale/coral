=== Dump BINARY_TAG
x86_64-slc6-gcc62-dbg

=== Dump CC
/cvmfs/sft.cern.ch/lcg/contrib/gcc/6.2.0native/x86_64-slc6/bin/gcc

=== Dump CORAL_BINARY_TAG_HASH
696

=== Dump CORALSYS
/home/cdelort/CORAL/trunk/${BINARY_TAG}

=== Dump CXX
/cvmfs/sft.cern.ch/lcg/contrib/gcc/6.2.0native/x86_64-slc6/bin/g++

=== Dump LCG_releases_base
/cvmfs/sft.cern.ch/lcg/releases/LCG_87

=== Dump LD_LIBRARY_PATH
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/libunwind/5c2cade/${BINARY_TAG}/lib
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/gperftools/2.5/${BINARY_TAG}/lib
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/igprof/5.9.16/${BINARY_TAG}/lib
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/oracle/11.2.0.3.0/${BINARY_TAG}/lib
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/expat/2.0.1/${BINARY_TAG}/lib
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/frontier_client/2.8.19/${BINARY_TAG}/lib
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/sqlite/3110100/${BINARY_TAG}/lib
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/mysql/5.7.11/${BINARY_TAG}/lib
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/XercesC/3.1.3/${BINARY_TAG}/lib
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/CppUnit/1.12.1_p1/${BINARY_TAG}/lib
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/Boost/1.62.0/${BINARY_TAG}/lib
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/Python/2.7.10/${BINARY_TAG}/lib
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/libaio/0.3.110-1/${BINARY_TAG}/lib
/home/cdelort/CORAL/trunk/${BINARY_TAG}/lib
/cvmfs/sft.cern.ch/lcg/contrib/gcc/6.2.0native/x86_64-slc6/lib64

=== Dump MANPATH
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/QMtest/2.4.1/${BINARY_TAG}/share/man
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/libunwind/5c2cade/${BINARY_TAG}/share/man
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/gperftools/2.5/${BINARY_TAG}/share/man
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/valgrind/3.11.0/${BINARY_TAG}/share/man
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/expat/2.0.1/${BINARY_TAG}/man
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/sqlite/3110100/${BINARY_TAG}/share/man
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/mysql/5.7.11/${BINARY_TAG}/man
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/CppUnit/1.12.1_p1/${BINARY_TAG}/share/man
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/Python/2.7.10/${BINARY_TAG}/share/man
/usr/share/man/overrides
/usr/share/man/en
/usr/share/man
/usr/local/share/man

=== Dump NLS_LANG
american_america.WE8ISO8859P1

=== Dump ORA_FPU_PRECISION
EXTENDED

=== Dump PATH
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/QMtest/2.4.1/${BINARY_TAG}/bin
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/gperftools/2.5/${BINARY_TAG}/bin
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/igprof/5.9.16/${BINARY_TAG}/bin
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/valgrind/3.11.0/${BINARY_TAG}/bin
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/oracle/11.2.0.3.0/${BINARY_TAG}/bin
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/expat/2.0.1/${BINARY_TAG}/bin
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/frontier_client/2.8.19/${BINARY_TAG}/bin
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/sqlite/3110100/${BINARY_TAG}/bin
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/mysql/5.7.11/${BINARY_TAG}/bin
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/XercesC/3.1.3/${BINARY_TAG}/bin
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/Python/2.7.10/${BINARY_TAG}/bin
/home/cdelort/CORAL/trunk/${BINARY_TAG}/bin
/home/cdelort/CORAL/trunk/${BINARY_TAG}/tests/bin
/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.6.0/Linux-x86_64/bin
/cvmfs/sft.cern.ch/lcg/contrib/ninja/1.4.0/x86_64-slc6
/usr/local/bin
/bin
/usr/bin

=== Dump PWD
/home/cdelort/CORAL/trunk/cmake/.internal

=== Dump PYTHONPATH
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/QMtest/2.4.1/${BINARY_TAG}/lib/python2.7/site-packages
/home/cdelort/CORAL/trunk/${BINARY_TAG}/python
/home/cdelort/CORAL/trunk/${BINARY_TAG}/lib
/home/cdelort/CORAL/trunk/${BINARY_TAG}/tests/bin

=== Dump QMTEST_CLASS_PATH
/home/cdelort/CORAL/trunk/${BINARY_TAG}/CoralTest/qmtest

=== Dump TNS_ADMIN
/afs/cern.ch/sw/lcg/app/releases/CORAL/internal/oracle/admin

=== Dump VALGRIND_LIB
/cvmfs/sft.cern.ch/lcg/releases/LCG_87/valgrind/3.11.0/${BINARY_TAG}/lib/valgrind

=== Dump VERBOSE
1

